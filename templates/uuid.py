import uuid
from faker import Faker


class UUID:
    def validate(string):
        if isinstance(string, UUID):
            return True
        if not isinstance(string, str):
            return False
        try:
            uuid.UUID(string)
            return True
        except ValueError:
            return False


class Factory:
    def __init__(self):
        self.faker = Faker("fr_FR")
        pass

    def seed(self, seed):
        Faker.seed(seed)
        self.faker = Faker("fr_FR")

    def generate(self, type):
        # This is generated code dw :)
        if type == "uuid":
            return self.faker.uuid4()
        if type == "str":
            return self.faker.word()
        if type == "int":
            return self.faker.random_int()
        if type == "float":
            return self.faker.random_number()
        if type == "bool":
            return self.faker.boolean()
        if type == "date":
            return self.faker.date()
        if type == "datetime":
            return self.faker.date_time()
        if type == "time":
            return self.faker.time()
        if type == "email":
            return self.faker.email()
        if type == "name":
            return self.faker.name()
        if type == "address":
            return self.faker.address()
        if type == "phone_number":
            return self.faker.phone_number()
        if type == "company":
            return self.faker.company()
        if type == "text":
            return self.faker.text()
        if type == "paragraph":
            return self.faker.paragraph()
        if type == "sentence":
            return self.faker.sentence()
        if type == "word":
            return self.faker.word()
        if type == "url":
            return self.faker.url()
        if type == "ipv4":
            return self.faker.ipv4()
        if type == "ipv6":
            return self.faker.ipv6()
        if type == "mac_address":
            return self.faker.mac_address()
        if type == "slug":
            return self.faker.slug()
        if type == "color":
            return self.faker.color()
        if type == "file_name":
            return self.faker.file_name()
        if type == "file_path":
            return self.faker.file_path()
        if type == "image_url":
            return self.faker.image_url()
        if type == "user_name":
            return self.faker.user_name()
        if type == "password":
            return self.faker.password()
        if type == "sha1":
            return self.faker.sha1()
        if type == "sha256":
            return self.faker.sha256()
        if type == "md5":
            return self.faker.md5()
        if type == "siren" or type == "SIREN":
            return self.faker.siren()
        if type == "null":
            return None
        return None


factory = Factory()

from ..uuid import UUID, factory
from ..savesql import s


class _Classname:
    def __init__(
        self,
        # _args
    ):
        # _fields_validation
        # _fields

        self.__types__ = {
            # _types
        }
        self.__tablename__ = "_classname"
        self.__saved_state__ = {}

        s.follow(self)
        pass

    # _getters

    # _setters

    # _getters_for_links

    def getFields(self, includeNone=False):
        fields = [field for field in self.__dict__ if not field.startswith(
            "__") and (includeNone or self.__dict__[field] is not None)]
        return fields

    def __str__(self):
        line = f"_Classname<"
        fields = self.getFields()
        for field in fields:
            line += f"{field[1:]}: {self.__dict__[field]}" + " ; "
        line = line[:-2] + ">"
        return line

    def generate(
        # _args
    ):
        # _generate
        pass  # _to_delete

    def generateOnlyRequired(
        # _args_only_required
    ):
        # _generate_only_required
        pass  # _to_delete

    def save_state(self):
        self.__saved_state__ = {field: self.__dict__[
            field] for field in self.getFields(True)}

    def sql_insert(self):
        line = f"INSERT INTO _classname ("
        fields = self.getFields()
        for field in fields:
            line += f"{field[1:]}, "
        line = line[:-2] + ")\nVALUES ("
        for field in fields:
            value = self.__dict__[field]
            if isinstance(value, str):
                line += f"'{value}', "
            elif isinstance(value, UUID):
                line += f"'{value}', "
            else:
                line += f"{value}, "
        line = line[:-2] + ");\n\n"
        return line

    def sql_update(self, full=False):
        line = f"UPDATE _classname\nSET "
        fields = self.getFields()
        for field in fields:
            if full or self.__saved_state__[field] != self.__dict__[field]:
                value = self.__dict__[field]
                if isinstance(value, str):
                    line += f"{field[1:]} = '{value}', "
                elif isinstance(value, UUID):
                    line += f"{field[1:]} = '{value}', "
                else:
                    line += f"{field[1:]} = {value}, "
        line = line[:-2] + "\nWHERE "
        line += f"id = '{self._id}';\n\n"
        return line

from pythonDBConnect import DBConnect


class SaveSql:
    def __init__(self, file_path="", writeOnChange=False):
        self.file_path = file_path
        self.writeOnChange = writeOnChange
        self.text = ""
        self.objects = []
        self.repository = {}

    def init(self, file_path, writeOnChange=None):
        self.file_path = file_path
        self.writeOnChange = writeOnChange if writeOnChange is not None else self.writeOnChange
        return self

    def clear(self):
        self.text = ""
        self.objects = []

    def write(self, text):
        self.text += text

    def follow(self, obj):
        self.objects.append(obj)
        self.repository.setdefault(obj.__tablename__, []).append(obj)
        if self.writeOnChange:
            self.write(obj.sql_insert())
            obj.save_state()
        return self.objects[-1]

    def insert_sql(self, obj):
        if self.writeOnChange:
            self.write(obj.sql_insert())
            obj.save_state()

    def update_sql(self, obj):
        if self.writeOnChange:
            self.write(obj.sql_update())
            obj.save_state()

    def write_all(self):
        for obj in self.objects:
            self.write(obj.sql_insert() + "\n")
            obj.save_state()

    def save(self):
        if not self.writeOnChange:
            self.write_all()

        with open(self.file_path, 'w') as f:
            f.write(self.text)

    def execute(self):
        if not self.writeOnChange:
            self.write_all()

        print("\nExecuting SQL:")
        print(self.text)
        print("\n")

        DBConnect().execute(self.text)

        return self.text

    # _getters_for_repository


s = SaveSql()

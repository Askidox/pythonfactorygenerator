import os
import psycopg2

# Database connection details


class DBConnect:
    def __init__(self):
        self.dbname = os.getenv("BACKEND_POSTGRES_DB")
        self.user = os.getenv("BACKEND_POSTGRES_USER")
        self.password = os.getenv("BACKEND_POSTGRES_PASSWORD")
        self.host = "localhost"  # defaults to localhost
        self.port = os.getenv("BACKEND_POSTGRES_PORT") or "5432"

    def connect(self):
        return psycopg2.connect(dbname=self.dbname, user=self.user,
                                password=self.password, host=self.host, port=self.port)

    def execute(self, sql):
        conn = self.connect()
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        cur.close()
        conn.close()

    def fetch(self, sql):
        conn = self.connect()
        cur = conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        conn.close()
        return rows

    def close(self):
        conn = self.connect()
        conn.close()

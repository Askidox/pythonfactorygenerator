import json
from pythonDBConnect import DBConnect

databaseLabels = []
database = []


def parseType(string):
    if string == "uniqueidentifier":
        return "uuid"
    if string == "nvarchar" or string == "varchar" or string == "charcter varying":
        return "str"
    if string == "entity" or string == "bigint" or string == "smallint":
        return "int"
    if string == "datetime" or string == "datetime2" or string == "datetimeoffset":
        return "datetime"
    if string == "bit":
        return "bool"
    if string == "decimal":
        return "float"
    return string


def parseRowToDict(row):
    return {
        "table_name": row[0],
        "column_name": row[1],
        "is_nullable": row[2],
        "data_type": parseType(row[3]),
        "character_maximum_length": row[4],
        "numeric_precision": row[5]
    }


def parse_json_file(filename):
    # Open the file in read mode
    try:
        with open(filename, "r") as json_file:
            # Read the entire file content as a string
            data = json_file.read()
    except FileNotFoundError:
        raise FileNotFoundError(f"Error: File not found - {filename}")

    return data.split("\n")


# postgresql
sql = "SELECT table_name, column_name, is_nullable, udt_name, character_maximum_length, numeric_precision \
    FROM information_schema.columns \
    WHERE table_schema = 'public' \
    ORDER BY  table_name, is_nullable"
rows = DBConnect().fetch(sql)

# SIM
# rows = parse_json_file("table_tmp")

# Print results
for row in rows:
    row = parseRowToDict(row)
    # row = parseRowToDict(row.split(","))
    if row["table_name"] not in databaseLabels:
        databaseLabels.append(row["table_name"])
        database.append({
            "table_name": row["table_name"],
            "columns": []
        })
    index = databaseLabels.index(row["table_name"])
    col = {
        "label": row["column_name"],
        "type": row["data_type"],
        "length": row["character_maximum_length"] or row["numeric_precision"],
        "is_nullable": row["is_nullable"] == "YES",
    }
    if row["column_name"] == "id":
        database[index]["columns"].insert(0, col)
    else:
        database[index]["columns"].append(col)

database = {
    "tables": database,
    "tablesNames": databaseLabels
}

# print(database)

with open("data.json", "w") as outfile:
    # Convert dictionary to JSON string and write to file
    json.dump(database, outfile)

print("Done!")

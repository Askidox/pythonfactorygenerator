import os
import shutil
import re
import json
from termcolor import colored

########################################
# # Get Data
########################################

with open("data.json", "r") as file:
    data = json.load(file)

########################################
# # Read templates
########################################

with open("templates/entity.py", "r") as file:
    lines = file.readlines()

with open("templates/savesql.py", "r") as file:
    savesqlLines = file.readlines()

########################################
# # Generate
########################################

# Path to the folder you want to create/recreate
folder_path = "generated"

# Check if the folder exists
if os.path.exists(folder_path):
    # Folder exists, so remove it
    shutil.rmtree(folder_path)

# Create the folder
os.makedirs(folder_path, exist_ok=True)

# Subfolders
os.makedirs(f"{folder_path}/entity", exist_ok=True)


class Intendation:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value

    @property
    def get(self):
        return self.value

    def getIf(self, value, condition):
        return self.value if condition(value) else ""

    @property
    def incr(self):
        return Intendation(self.value + "    ")

    @property
    def decr(self):
        return Intendation(self.value[:-4])


class Lines:
    def __init__(self, intendation=Intendation("")):
        self.value = ""
        self.intendation = intendation

    def __str__(self):
        return self.value

    def add(self, value):
        self.value += "\n" + self.intendation.get + value

    def addWithIncrIf(self, value, condition):
        self.value += self.intendation.getIf(self.value, condition) + value

    def addWithoutIncr(self, value):
        self.value += "\n" + self.intendation.get + value

    def addIncr(self, value):
        self.value += "\n" + self.intendation.incr.get + value

    def addToLine(self, value):
        self.value += value

    def addWII(self, value):
        if len(self.value) != 0:
            self.value += "\n"
            self.value += self.intendation.get
        self.value += value

    def incr(self):
        self.intendation = self.intendation.incr

    def decr(self):
        self.intendation = self.intendation.decr

    def newLine(self, n=1):
        self.value += "\n" * n


tlt = str.title


def toCamelCase(s, title=False):
    first = s.split("_")[0].lower() if not title else tlt(s.split("_")[0])
    remain = "".join([tlt(w) for w in s.split("_")[1:]])
    return first + remain


def writeArgs(d, intendation, *_):
    lines = Lines(intendation)

    for col in d['columns']:
        lines.addWII(f"{toCamelCase(col['label'])} = None,")

    return lines.value


def writeArgsOnlyRequired(d, intendation, *_):
    lines = Lines(intendation)

    for col in d['columns']:
        if not col['is_nullable']:
            lines.addWII(f"{toCamelCase(col['label'])} = None,")

    return lines.value


def getTypeValidation(col, lines):
    label = toCamelCase(col["label"])
    if label.lower() == "id":
        lines.add(f"if {label} is None:")
        lines.addIncr(f"{label} = factory.faker.uuid4()")

    if col["type"] == "uuid":
        lines.add(f"if not UUID.validate({label})")
        if col["is_nullable"]:
            lines.addToLine(" and " + label + " is not None")
        lines.addToLine(":")
        lines.addIncr(
            f"raise ValueError(f\"{label}: " + "{" + label + "} is not a valid UUID.\")")
    elif not col["is_nullable"]:
        lines.add(f"if {label} is None:")
        lines.addIncr(
            f"raise ValueError(\"{label} cannot be None.\")")

    return lines.value


def writeFieldsValidation(d, intendation, *_):
    lines = Lines(intendation)
    for col in d["columns"]:
        getTypeValidation(col, lines)
    return lines.value


def writeFields(d, intendation, *_):
    lines = Lines(intendation)
    for col in d["columns"]:
        lines.add(f"self._{col['label']} = {toCamelCase(col['label'])}")
    return lines.value


def writeTypesVar(d, intendation, *_):
    lines = Lines(intendation)
    lines.addToLine("self.__types__ = {")
    lines.incr()
    for col in d["columns"]:
        lines.add(f"{col['label']}: \"{col['type']}\",")
    lines.decr()
    lines.add("}")
    return lines.value


def linesNotEmpty(x): return len(x) != 0


def writeSetters(d, intendation, *_):
    lines = Lines(intendation)
    for col in d["columns"]:
        label = toCamelCase(col["label"])
        lines.addWithIncrIf(f"@{label}.setter", linesNotEmpty)
        lines.add(f"def {label}(self, {label}):")
        if col["label"].lower() == "id":
            lines.addIncr(
                f"raise ValueError(\"{label} should not be changed.\")")
            lines.newLine(2)
            continue
        lines.incr()
        if not col["is_nullable"]:
            lines.add(f"if {label} is None:")
            lines.addIncr(
                f"raise ValueError(\"{label} cannot be None.\")")
        getTypeValidation(col, lines)
        lines.add(f"self._{col['label']} = {label}")
        lines.add(f"s.update_sql(self)")
        lines.decr()
        lines.newLine(2)
    return lines.value


def writeGetters(d, intendation, *_):
    lines = Lines(intendation)
    for col in d["columns"]:
        lines.addWithIncrIf(f"@property", linesNotEmpty)
        lines.add(f"def {toCamelCase(col['label'])}(self):")
        lines.addIncr(f"return self._{col['label']}")
        lines.newLine(2)
    return lines.value


def getLinkedEntities(d):
    return [col["label"][:-3] for col in d["columns"] if col["label"].endswith("_id")]


def writeGenerate(d, intendation, *_):
    lines = Lines(intendation)
    links = getLinkedEntities(d)
    columns = [col
               for col in d["columns"] if not col["label"].endswith("id")]

    lines.addWII(writeImports(d, intendation))

    for col in columns:
        lines.addWII(
            f"{toCamelCase(col['label'])} = {toCamelCase(col['label'])} or factory.generate(\"{(col['label'])}\") or factory.generate(\"{(col['type'])}\")")
    for link in links:
        lines.addWII(f"if {toCamelCase(link)}_id is None:")
        lines.incr()
        if link in data["tablesNames"]:
            linkClass = toCamelCase(link, True)
            lines.add(f"{link}_id = {linkClass}.generate().id")
            lines.add(
                f"print(f\"Generated {linkClass} during {toCamelCase(d['table_name'], True)} generation\")")
        # if the link is not nullable
        elif not [col for col in d['columns'] if col['label'] == link+"_id"][0]["is_nullable"]:
            lines.add(f"{link}_id = factory.faker.uuid4()")
            lines.add(
                f"print(f\"" + colored(f"Warning: Generated random uuid for {link}_id during {toCamelCase(d['table_name'], True)} generation as it is a mandatory field", color="yellow")+"\")")
            lines.add(
                f"print(f\"\t\t\tThis may generate sql errors. Please specify an existing object uuid for {link}_id\")")
        else:
            lines.add(
                f"print(f\"Info: No generation for {link}_id during {toCamelCase(d['table_name'], True)} generation as it isn't a mandatory field\")")
        lines.decr()

    lines.addWII(f"return {toCamelCase(d['table_name'], True)}(")
    for col in d['columns']:
        if col["label"].lower() != "id":  # id is generated in constructor
            lines.addIncr(
                f"{toCamelCase(col['label'])} = {toCamelCase(col['label'])},")
    lines.add(f")")

    return lines.value


def writeGenerateOnlyRequired(d, intendation, *_):
    lines = Lines(intendation)
    links = getLinkedEntities(d)
    columns = [col
               for col in d["columns"] if not col["label"].endswith("id") and not col["is_nullable"]]

    lines.addWII(writeImports(d, intendation))

    for col in columns:
        lines.addWII(
            f"{toCamelCase(col['label'])} = {toCamelCase(col['label'])} or factory.generate(\"{(col['label'])}\") or factory.generate(\"{(col['type'])}\")")
    for link in links:
        if [col for col in d['columns'] if col['label'] == link+"_id"][0]["is_nullable"]:
            continue
        lines.addWII(f"if {link}_id is None:")
        lines.incr()
        if link in data["tablesNames"]:
            linkClass = toCamelCase(link, True)
            lines.add(f"{link}_id = {linkClass}.generate().id")
            lines.add(
                f"print(f\"Generated {linkClass} during {toCamelCase(d['table_name'], True)} generation\")")
        # if the link is not nullable
        elif not [col for col in d['columns'] if col['label'] == link+"_id"][0]["is_nullable"]:
            lines.add(f"{link}_id = factory.faker.uuid4()")
            lines.add(
                f"print(f\"" + colored(f"Warning: Generated random uuid for {link}_id during {toCamelCase(d['table_name'], True)} generation as it is a mandatory field", color="yellow")+"\")")
            lines.add(
                f"print(f\"\t\t\tThis may generate sql errors. Please specify an existing object uuid for {link}_id\")")
        lines.decr()

    lines.addWII(f"return {toCamelCase(d['table_name'], True)}(")
    for col in d['columns']:
        if col["label"].lower() != "id" and not col['is_nullable']:  # id is generated in constructor
            lines.addIncr(
                f"{toCamelCase(col['label'])} = {toCamelCase(col['label'])},")
    lines.add(f")")

    return lines.value


def writeImports(d, i, *_):
    linked = getLinkedEntities(d)
    lines = Lines(i)
    for link in linked:
        if link in data["tablesNames"]:
            # lines.add(f"from {link} import {toCamelCase(link, True)}")
            lines.addWII(f"from .. import {toCamelCase(link, True)}")
        else:
            lines.addWII(f"# No link found for {link}_id")
    return lines.value


def writeGettersForRepository(d, i, *_):
    lines = Lines(i)
    for table in data["tablesNames"]:
        lines.addWII(f"@property")
        lines.add(f"def {toCamelCase(table)}s(self):")
        lines.addIncr(f"return self.repository.get(\"{table}\", [])")
        lines.newLine()
    for table in data["tablesNames"]:
        # lines.addWII(f"@property")
        lines.add(f"def get{toCamelCase(table, True)}(self, uuid):")
        lines.incr()
        lines.add(
            f"{table} = [{table} for {table} in self.{toCamelCase(table)}s if {table}.id == uuid]")
        lines.add(f"if len({table}) == 0:")
        lines.addIncr(f"return None")
        lines.add(f"return {table}[0]")
        lines.newLine()
        lines.decr()
    return lines.value


def writeGettersForLinks(d, i, *_):
    lines = Lines(i)
    for col in d["columns"]:
        if not col["label"].endswith("_id"):
            continue

        link = col["label"][:-3]
        if link in data["tablesNames"]:
            lines.addWII(f"@property")
            lines.add(f"def {toCamelCase(link)}(self):")
            lines.addIncr(
                f"return s.get{toCamelCase(link, True)}(self.{col['label']})")
            lines.newLine()
    return lines.value


mapper = {
    "_classname": lambda d, *_: d["table_name"],
    "_Classname": lambda d, *_: toCamelCase(d["table_name"], True),
    "# _args_only_required": writeArgsOnlyRequired,
    "# _args": writeArgs,
    "# _fields_validation": writeFieldsValidation,
    "# _fields": writeFields,
    "# _getters_for_repository": writeGettersForRepository,
    "# _getters_for_links": writeGettersForLinks,
    "# _setters": writeSetters,
    "# _getters": writeGetters,
    "# _generate_only_required": writeGenerateOnlyRequired,
    "# _generate": writeGenerate,
    "# _types_var": writeTypesVar,
    "# _imports": writeImports,
}

########################################
# # Model generation
########################################


def writeLine(lines):
    for line in lines:
        if "# _to_delete" in line:
            continue
        i = Intendation((len(line) - len(line.lstrip())) * " ")
        # check if the line contains a mapper key
        for key, value in mapper.items():
            if key in line:
                line = re.sub(key, value(table, i), line)
        file.write(line)


for table in data["tables"]:
    # Generate a file for each model
    file_name = f"{table['table_name']}.py"
    file_path = os.path.join(folder_path, "entity", file_name)

    with open(file_path, "w") as file:
        writeLine(lines)
    os.chmod(file_path, 0o444)

    # print(f"File '{file_name}' generated at '{file_path}'.")

########################################
# # Utils generation
########################################

shutil.copy("templates/uuid.py", f"{folder_path}/uuid.py")
os.chmod(f"{folder_path}/uuid.py", 0o444)
shutil.copy("pythonDBConnect.py", f"{folder_path}/pythonDBConnect.py")
os.chmod(f"{folder_path}/pythonDBConnect.py", 0o444)

# shutil.copy("templates/savesql.py", f"{folder_path}/savesql.py")
file_name = "savesql.py"
file_path = os.path.join(folder_path, file_name)
with open(file_path, "w") as file:
    writeLine(savesqlLines)
os.chmod(file_path, 0o444)


def writeImports(d, *_):
    return f"from .entity.{d['table_name']} import {toCamelCase(d['table_name'], True)}"

########################################
# # __init__ generation
########################################


file_name = "__init__.py"
file_path = os.path.join(folder_path, file_name)
with open(file_path, "w") as file:
    lines = Lines()
    for table in data["tables"]:
        lines.add(writeImports(table) + "\n")
    lines.add("from .savesql import SaveSql")
    lines.add("from .uuid import UUID")

    file.write(lines.value)

os.chmod(file_path, 0o444)

print(f"Generated {len(data['tables']) + 3} files.")
